import Phaser, { Scenes } from "phaser";
import PlayScene from './scenes/play_scene'
import MenuScene from './scenes/menu_scene'
import PreloadScene from './scenes/preload_scene'
import ScoreScene from "./scenes/score_scene";
import PauseScene from "./scenes/pause_scene";
import GameOverScene from "./scenes/game_over_scene.js"

const WIDTH = 400;
const HEIGHT = 600;
const BIRD_POSITION = { x: WIDTH * 0.1, y: HEIGHT / 2 }
const SHARED_CONFIG = {
  width: WIDTH,
  height: HEIGHT,
  startPosition: BIRD_POSITION
}

const scenes = [PreloadScene, MenuScene, PlayScene, ScoreScene, PauseScene, GameOverScene];
const createScene = (Scene) => new Scene(SHARED_CONFIG)
const initScenes = () => scenes.map(createScene)

const config = {
  // WebGL
  type: Phaser.AUTO,
  ...SHARED_CONFIG,
  pixelArt: true,
  physics: {
    // Aracede physics plugin, manages physics simulation
    default: 'arcade',
    arcade: {
      debug: true
    }
  },
  scene: initScenes()
}

new Phaser.Game(config);

import BaseScene from './base_scene';

function PauseScene(config) {
  const self = new BaseScene('PauseScene', config); // this;

  self.menu = [
    { scene: 'PlayScene', text: 'Continue' },
    { scene: 'MenuScene', text: 'Exit' },
  ]

  self.create = function () {
    // Call Basescene.create method directly, avoid
    // calling indefinietly the self.create method
    // which we define at the beginning of this
    // function and that shadow the already defined
    // create function
    self.__proto__.create.call(this);

    // using bind here to provide context in setupmenuevents
    // aka: this
    // but since we have a reference for self already we don't
    // need this trick here
    // self.createPause(self.menu, setupPauseEvents.bind(self))

    self.createMenu(self.menu, setupPauseEvents)
  }

  return self;

  function setupPauseEvents(menuItem) {
    const textGO = menuItem.textGO;
    textGO.setInteractive()

    textGO.on('pointerover', () => {
      textGO.setStyle({ fill: "#ff0" })
    })

    textGO.on('pointerout', () => {
      textGO.setStyle({ fill: "#fff" })
    })

    textGO.on('pointerup', () => {
      if (menuItem.scene && menuItem.text === 'Continue') {
	self.scene.stop();
	self.scene.resume(menuItem.scene)
      } else if (menuItem.scene && menuItem.text === 'Exit') {
	self.scene.stop('PlayScene')
	self.scene.start(menuItem.scene)
      }
      console.log("clicking on some options")
    })
  }
}

// need to find out if we use this path how params like 'PauseScene' can be passed to prototype
//PauseScene.prototype = Phaser.Scene

export default PauseScene;

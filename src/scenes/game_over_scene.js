import BaseScene from './base_scene';

function GameOverScene(config) {
  const self = new BaseScene('GameOverScene', config); // this;

  self.menu = [
    { scene: 'PlayScene', text: 'Restart' },
    { scene: 'MenuScene', text: 'Exit' },
  ]

  self.create = function () {
    // Call BaseScene.create method directly, avoid
    // calling indefinietly the self.create method
    // which we define at the beginning of this
    // function and that shadow the already defined
    // create function
    self.__proto__.create.call(this);

    self.createMenu(self.menu, setupPauseEvents)
  }

  return self;

  function setupPauseEvents(menuItem) {
    const textGO = menuItem.textGO;
    textGO.setInteractive()

    textGO.on('pointerover', () => {
      textGO.setStyle({ fill: "#ff0" })
    })

    textGO.on('pointerout', () => {
      textGO.setStyle({ fill: "#fff" })
    })

    textGO.on('pointerup', () => {
      if (menuItem.scene && menuItem.text === 'Restart') {
	self.scene.stop();
	self.scene.start(menuItem.scene)
      } else if (menuItem.scene && menuItem.text === 'Exit') {
	self.scene.stop('PlayScene')
	self.scene.start(menuItem.scene)
      }
      console.log("clicking on some options")
    })
  }
}

// need to find out if we use this path how params like 'PauseScene' can be passed to prototype
//PauseScene.prototype = Phaser.Scene

export default GameOverScene;

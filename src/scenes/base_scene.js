import Phaser from 'phaser'

// As PlayScene still in ES6 syntaxt this needs to follow
// class syntax howerer the "newer" Adam based inheritance
// syntax is flexible enough to handle parent regardless it
// is a class or function
class BaseScene extends Phaser.Scene {
  constructor(key, config) {
    super(key)
    this.config = config
    this.screenCenter = [config.width / 2, config.height / 2]
    this.fontSize = 32;
    this.lineHeight = 42;
    this.fontOptions = { fontSize: `${this.fontSize}px`, fill: '#fff' };
  }

  create() {
    this.add.image(0,0, 'sky').setOrigin(0,0)

    if (this.config.canGoBack) {
      const backButton = this
	.add.image(this.config.width - 10, this.config.height - 10, 'back')
	.setOrigin(1).setScale(2).setInteractive();
      backButton.on('pointerup', () => this.scene.start('MenuScene'));
    }
  }

  createMenu(menu, setupMenuEvents) {
    let lastMenuPosition = 0
    menu.forEach(menuItem => {
      const menuPosition = [this.screenCenter[0], this.screenCenter[1] + lastMenuPosition];
      menuItem.textGO = this.add.text(...menuPosition, menuItem.text, this.fontOptions).setOrigin(0.5, 1);
      lastMenuPosition += this.lineHeight;
      setupMenuEvents(menuItem)
    })
  }
}

export default BaseScene;

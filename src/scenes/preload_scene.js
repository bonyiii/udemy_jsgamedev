import BaseScene from './base_scene';

function PreloadScene(config) {
  const self = new BaseScene('PreloadScene', config);

  self.preload = function() {
    self.load.image('sky', 'assets/sky.png');
    self.load.spritesheet('bird', 'assets/birdSprite.png', {
      frameWidth: 16, frameHeight: 16
    });
    self.load.image('pipe', 'assets/pipe.png');
    self.load.image('pause', 'assets/pause.png');
    self.load.image('back', 'assets/back.png');
  }

  self.create = function() {
    self.scene.start('MenuScene')
  }

  return self;
}

export default PreloadScene;

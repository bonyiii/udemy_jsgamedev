import BaseScene from './base_scene';

function MenuScene(config) {
  const self = new BaseScene('MenuScene', config); // this;

  self.menu = [
    { scene: 'PlayScene', text: 'Play' },
    { scene: 'ScoreScene', text: 'Score' },
    { scene: null, text: 'Exit' }
  ]

  self.create = function () {
    // Call Basescene.create method directly, avoid
    // calling indefinietly the self.create method
    // which we define at the beginning of this
    // function and that shadow the already defined
    // create function
    self.__proto__.create.call(this);

    // using bind here to provide context in setupmenuevents
    // aka: this
    // but since we have a reference for self already we don't
    // need this trick here
    // self.createMenu(self.menu, setupMenuEvents.bind(self))

    self.createMenu(self.menu, setupMenuEvents)
  }

  return self;

  function setupMenuEvents(menuItem) {
    const textGO = menuItem.textGO;
    textGO.setInteractive()

    textGO.on('pointerover', () => {
      textGO.setStyle({ fill: "#ff0" })
    })

    textGO.on('pointerout', () => {
      textGO.setStyle({ fill: "#fff" })
    })

    textGO.on('pointerup', () => {
      menuItem.scene && self.scene.start(menuItem.scene)

      if (menuItem.text == 'Exit') {
	self.game.destroy(true)
      }
    })
  }
}

// need to find out if we use this path how params like 'MenuScene' can be passed to prototype
//MenuScene.prototype = Phaser.Scene

export default MenuScene;

import BaseScene from './base_scene';

function ScoreScene(config) {
  const self = new BaseScene('ScoreScene', { ...config, canGoBack: true }); // this;

  self.create = function () {
    self.__proto__.create.call(this);

    const bestScore = localStorage.getItem('bestScore');
    self.add.text(...self.screenCenter, `Best score: ${bestScore}`, self.fontOptions)
	.setOrigin(0.5)
  }

  return self;

  function setupMenuEvents(menuItem) {
    const textGO = menuItem.textGO;
    textGO.setInteractive()

    textGO.on('pointerover', () => {
      textGO.setStyle({ fill: "#ff0" })
    })

    textGO.on('pointerout', () => {
      textGO.setStyle({ fill: "#fff" })
    })

    textGO.on('pointerup', () => {
      menuItem.scene && self.scene.start(menuItem.scene)

      if (menuItem.text == 'Exit') {
	self.game.destroy(true)
      }
    })
  }
}

// need to find out if we use this path how params like 'MenuScene' can be passed to prototype
//MenuScene.prototype = Phaser.Scene

export default ScoreScene;

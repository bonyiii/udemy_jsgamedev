import BaseScene from './base_scene';

const PIPES_TO_RENDER = 4;

class PlayScene extends BaseScene {
  constructor(config) {
    super('PlayScene', config)

    this.bird = null
    this.pipes = null
    this.isPaused = false

    this.pipeVerticalDistanceRange = [100, 250];
    this.pipeHorizontalDistanceRange = [500, 600];
    this.pipeHorizontalDistance = 0;
    this.flapVelocity = 250;

    this.difficulties = {
      'easy': {
	pipeHorizontalDistanceRange: [300, 400],
	pipeVerticalDistanceRange: [150, 200]
      },
      'normal': {
	pipeHorizontalDistanceRange: [280, 330],
	pipeVerticalDistanceRange: [140, 190]
      },
      'hard': {
	pipeHorizontalDistanceRange: [250, 310],
	pipeVerticalDistanceRange: [120, 150]
      }
    }

    this.score = 0;
    this.scoreText = ''
  }

  create() {
    this.currentDifficulty = 'easy'
    super.create()
    this.createBird();
    this.createPipes();
    this.createColliders();
    this.createScores();
    this.createPause();
    this.handleInputs();
    this.listenToEvents();

    this.anims.create({
      key: 'fly',
      frames: this.anims.generateFrameNumbers('bird', { start: 8, end: 15 }),
      frameRate: 8,
      repeat: -1
    })

    this.bird.play('fly');
  }

  update() {
    this.checkGameStatus()
    this.recyclePipes();
  }

  /*
     listenToEvents memeber method called multiple times, so we save the resume event listener
     into a member variable, and check if that's defined already if so, we don't subscribe to
     the same event multiple times
   */
  listenToEvents() {
    if (this.pauseEvent) { return; }

    this.pauseEvent = this.events.on('resume', () => {
      this.initialTime = 3;
      this.countDownText = this.add.text(...this.screenCenter, 'Fly in ' + this.initialTime, this.fontOptions).setOrigin(0.5)
      this.timedEvent = this.time.addEvent({
	delay: 500,
	callback: this.countDown,
	callbackScope: this,
	loop: true
      })
    })
  }

  countDown() {
    this.initialTime--;
    this.countDownText.setText(`Fly in ${this.initialTime}`)

    if (this.initialTime <= 0) {
      this.isPaused = false
      this.countDownText.setText();
      this.physics.resume();
      this.timedEvent.remove();
    }
  }

  createBG() {
    this.add.image(0, 0, 'sky').setOrigin(0, 0);
  }

  createBird() {
    this.bird = this
      .physics.add.sprite(this.config.startPosition.x, this.config.startPosition.y, 'bird')
      .setFlipX(true).setOrigin(0, 0).setScale(3);
    this.bird.setBodySize(this.bird.width, this.bird.height - 8)
    this.bird.body.gravity.y = 400;
    this.bird.setCollideWorldBounds()
  }

  createPipes() {
    this.pipes = this.physics.add.group();
    for (let i = 0; i < PIPES_TO_RENDER; i++) {
      const upperPipe = this.pipes.create(0, 0, 'pipe')
			    .setImmovable(true)
			    .setOrigin(0, 1);
      const lowerPipe = this.pipes.create(0, 0, 'pipe')
			    .setImmovable(true)
			    .setOrigin(0, 0);

      this.placePipes(upperPipe, lowerPipe);
    }
    this.pipes.setVelocityX(-200);
  }

  createColliders() {
    this.physics.add.collider(this.bird, this.pipes, this.gameOver, null, this);
  }

  createScores() {
    this.score = 0;
    const bestScore = localStorage.getItem('bestScore')
    this.scoreText = this.add.text(16, 16, `Score: ${0}`, { fontSize: '32px', fill: '#000' });
    this.add.text(16, 52, `Best score: ${bestScore}`, { fontSize: '18px', fill: '#000' });
  }

  createPause() {
    this.isPaused = false
    const pauseButton = this
      .add
      .image(this.config.width - 10, this.config.height - 10, 'pause')
      .setInteractive()
      .setScale(3)
      .setOrigin(1, 1);

    pauseButton.on('pointerdown', () => {
      this.isPaused = true
      this.physics.pause();
      this.scene.pause();
      this.scene.launch('PauseScene');
    })
  }

  handleInputs() {
    this.input.on('pointerdown', this.flap, this)
    this.input.keyboard.on('keydown_SPACE', this.flap, this)
  }

  checkGameStatus() {
    if (this.bird.body.y <= 0) {
      console.log("Yoy have hit the top")
      this.gameOver();
    } else if (this.bird.getBounds().bottom >= this.config.height) {
      console.log("Yoy have hit the bottom")
      this.gameOver();
    }
  }

  placePipes(upperPipe, lowerPipe) {
    const difficulty = this.difficulties[this.currentDifficulty]
    const rightMostX = this.getRightmostPipe()
    const pipeVerticalDistance = Phaser.Math.Between(...difficulty.pipeVerticalDistanceRange);
    const pipeVerticalPosition = Phaser.Math.Between(20, this.config.height - 20 - pipeVerticalDistance);
    const pipeHorizontalDistance = Phaser.Math.Between(...difficulty.pipeHorizontalDistanceRange)

    upperPipe.x = rightMostX + pipeHorizontalDistance;
    upperPipe.y = pipeVerticalPosition;

    lowerPipe.x = upperPipe.x;
    lowerPipe.y = upperPipe.y + pipeVerticalDistance;
  }

  getRightmostPipe() {
    let rightMostX = 0;
    this.pipes.getChildren().forEach(function(pipe) {
      rightMostX = Math.max(pipe.x, rightMostX);
    })
    return rightMostX;
  }

  flap() {
    if (!this.isPaused) {
      this.bird.body.velocity.y = -this.flapVelocity;
    }
  }

  recyclePipes() {
    function pairs(list, myFunc, skip) {
      for(let i = 0; i < list.length; i += skip) {
	myFunc(list[i], list[i+skip-1]);
      }
    }
    const self = this
    pairs(this.pipes.getChildren(), function(upperPipe, lowerPipe) {
      if (upperPipe.getBounds().right < 0) {
	self.placePipes(upperPipe, lowerPipe);
	self.increaseScore();
	self.setBestScore();
	self.increaseDifficulty();
      }
    }, 2);
  }

  increaseDifficulty() {
    if (this.score === 1) {
      this.currentDifficulty = 'normal';
      console.log('difficult normal')
    }

    if (this.score === 3) {
      this.currentDifficulty = 'hard';
      console.log('difficult hard')
    }
  }

  setBestScore() {
    const bestScoreText = localStorage.getItem('bestScore')
    const bestScore = bestScoreText && parseInt(bestScoreText, 10);
    if (!bestScore || this.score > bestScore) {
      localStorage.setItem('bestScore', this.score);
    }
  }

  gameOver() {
    this.physics.pause();
    this.bird.setTint(0xf0000)

    this.setBestScore()
    this.scene.launch('GameOverScene');
    /* this.time.addEvent({
     *   delay: 1000,
     *   callback: () => {
       this.scene.restart();
     *   },
     *   loop: false
     * }) */
  }

  increaseScore() {
    this.score++;
    this.scoreText.setText(`Score: ${this.score}`)
  }
}

export default PlayScene;
